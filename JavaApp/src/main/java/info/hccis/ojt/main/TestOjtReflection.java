/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.ojt.main;

import info.hccis.ojt.model.jpa.OjtReflection;
import info.hccis.ojt.util.UtilityRest;
import java.util.Scanner;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author dswaminathan
 */
public class TestOjtReflection {

    //  final public static String MENU = "\nMain Menu \nA) Add student \n"
    //          + "V) View Students\n"
    //          + "X) eXit";

    private static final String URL_STRING = "http://localhost:8080/ojt/rest/ojtService/ojt/reflection";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        boolean endProgram = false;

        String jsonReturned = UtilityRest.getJsonFromRest(URL_STRING);
        //**************************************************************
        //Based on the json string passed back, loop through each json
        //object which is a json string in an array of json strings.
        //*************************************************************
        JSONArray jsonArray = new JSONArray(jsonReturned);
        //**************************************************************
        //For each json object in the array, show the first and last names
        //**************************************************************
        System.out.println("Here are the students");
        for (int currentIndex = 0; currentIndex < jsonArray.length(); currentIndex++) {
            JSONObject studentJson = jsonArray.getJSONObject(currentIndex);
            System.out.println(studentJson.getString("studentName") + " " + studentJson.getString("reflection"));

        }
    }

    /**
     * Create a student object by passing asking user for input.
     *
     * @return student
     * @author Divya Swaminathan
     *
     * public static OjtReflection createStudent() { OjtReflection newStudent =
     * new OjtReflection();
     *
     * System.out.println("Enter student id:");
     * newStudent.setStudentId(input.nextInt());
     *
     * System.out.println("Enter student Name:");
     * newStudent.setStudentName(input.next());
     *
     * System.out.println("Enter student reflection:");
     * newStudent.setReflection(input.next());
     *
     *
     *
     * return newStudent;
    }
     */
}
