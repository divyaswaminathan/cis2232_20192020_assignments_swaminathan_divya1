package info.hccis.ojt.model.jpa;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-11-15T12:49:01")
@StaticMetamodel(OjtReflection.class)
public class OjtReflection_ { 

    public static volatile SingularAttribute<OjtReflection, Integer> studentId;
    public static volatile SingularAttribute<OjtReflection, String> reflection;
    public static volatile SingularAttribute<OjtReflection, String> studentName;
    public static volatile SingularAttribute<OjtReflection, Integer> id;

}