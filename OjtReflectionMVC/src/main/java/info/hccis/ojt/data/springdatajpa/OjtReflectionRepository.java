/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.ojt.data.springdatajpa;

import info.hccis.ojt.entity.OjtReflection;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OjtReflectionRepository extends CrudRepository<OjtReflection, Integer> {

    List<OjtReflection> findById(int studentId);

}
