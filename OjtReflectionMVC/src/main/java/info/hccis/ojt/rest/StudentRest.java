/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.ojt.rest;

import com.google.gson.Gson;
import info.hccis.ojt.data.springdatajpa.OjtReflectionRepository;
import info.hccis.ojt.entity.OjtReflection;
import java.util.ArrayList;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author dswaminathan
 */
@Path("/ojtService")
public class StudentRest {
    
     @Resource
    private final OjtReflectionRepository ojtRep;

    /**
     * Note that dependency injection (constructor) is used here to provide the
     * CamperRepository object for use in this class.
     *
     * @param servletContext Context
     * @since 20181109
     * @author BJM
     */
    public StudentRest(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.ojtRep = applicationContext.getBean(OjtReflectionRepository.class);
    }
    
      @GET
    //@Produces({MediaType.APPLICATION_JSON}) 
    @Path("ojt/reflection")
    public Response getStudentReflection() {

        

        //Camper camper = CamperDAO.select(Integer.parseInt(camperId));
       ArrayList<OjtReflection> students = (ArrayList<OjtReflection>)ojtRep.findAll();

        if (students == null) {
            return Response.status(204).entity("{}").build();
        } else {
            Gson gson = new Gson();
            return Response.status(200).entity(gson.toJson(students)).build();
        }

    }
}
