/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.cis2232_assignment6_swaminathan_divya;

import java.util.Scanner;

/**
 *
 * @author dswaminathan
 */
public class Tennis {

    private static int numberMembers;

    public int getNumberMembers() {
        return numberMembers;
    }

    public void setNumberMembers(int numberMembers) {
        this.numberMembers = numberMembers;
    }

    public boolean isIsMember() {
        return isMember;
    }

    public void setIsMember(boolean isMember) {
        this.isMember = isMember;
    }

    public int getLesson() {
        return lesson;
    }

    public void setLesson(int lesson) {
        this.lesson = lesson;
    }

    private static final int ONE_MEMBER = 55;
    private static final int TWO_MEMBER = 30;
    private static final int THREE_MEMBER = 21;
    private static final int FOUR_MEMBER = 16;

    private static final int ONE_NONMEMBER = 60;
    private static final int TWO_NONMEMBER = 33;
    private static final int THREE_NONMEMBER = 23;
    private static final int FOUR_NONMEMBER = 18;

    private static boolean isMember;

    private static int lesson;
    private static double cost;
    
    public static void main(String[] args) {
         Scanner input = new Scanner(System.in);

        System.out.println("Welcome to the CIS Tennis Program");
        System.out.println("How many are in the group(1,2,3,4)");
        numberMembers = input.nextInt();
        System.out.println("Are you a member(Y/N)");
        String y = input.nextLine();
        if (y.equalsIgnoreCase("Y")) {
            isMember = true;
        } else {
            isMember = false;
        }
        System.out.println("How many hours do you want for your lesson ?");
        lesson = input.nextInt();
       calculateCost();
    }

    public void getInformation() {
      
            
    }

    public double getCost() {
        return cost;
    }

    public static void calculateCost()
    {
        switch (numberMembers) {
            case 1:
                if (isMember) {
                    cost =  ONE_MEMBER *lesson;
                } else {
                    cost =  ONE_NONMEMBER *lesson ;
                }
                break;
            case 2:
                if (isMember) {
                    cost =  TWO_MEMBER *lesson;
                } else {
                   cost =  TWO_NONMEMBER * lesson;
                }
                break;
            case 3:
                if (isMember) {
                    cost = THREE_MEMBER * lesson;
                } else {
                    cost = THREE_NONMEMBER * lesson;
                }
                break;
            case 4:
                if (isMember) {
                    cost = FOUR_MEMBER *lesson;
                } else {
                    cost =  FOUR_NONMEMBER *lesson;
                }
                break;

        }
    }
    

}
