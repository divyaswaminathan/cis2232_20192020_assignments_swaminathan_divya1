/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import info.hccis.cis2232_assignment6_swaminathan_divya.Tennis;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author dswaminathan
 */
public class Tennis_setNumberMembers {
    
    public Tennis_setNumberMembers() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    Tennis tennis;
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testNumberOfMember()
    {
        tennis = new Tennis();
        tennis.setNumberMembers(2);
        double result = tennis.getNumberMembers();
        assertEquals(2, result);
        //tennis.
    }
    @Test
    public void testMember3()
    {
        tennis = new Tennis();
        tennis.setNumberMembers(3);
        double result = tennis.getNumberMembers();
        assertEquals(3, result);
        //tennis.
    }
    @Test
    public void testNumberOfMember2()
    {
        tennis = new Tennis();
        tennis.setNumberMembers(2);
        tennis.setIsMember(true);
        tennis.setLesson(1);
        tennis.calculateCost();
        
        double result = tennis.getCost();
        assertEquals(30, result);
        //tennis.
    }
    @Test
    public void testNumberOfMember3()
    {
        tennis = new Tennis();
        tennis.setNumberMembers(3);
        tennis.setIsMember(true);
        tennis.setLesson(1);
        tennis.calculateCost();
        
        double result = tennis.getCost();
        assertEquals(21, result);
        //tennis.
    }
    @Test
    public void testNumberOfMember4()
    {
        tennis = new Tennis();
        tennis.setNumberMembers(4);
        tennis.setIsMember(true);
        tennis.setLesson(1);
        tennis.calculateCost();
        
        double result = tennis.getCost();
        assertEquals(16, result);
        //tennis.
    }
     @Test
    public void testNonMember2()
    {
        tennis = new Tennis();
        tennis.setNumberMembers(2);
        tennis.setIsMember(false);
        tennis.setLesson(1);
        tennis.calculateCost();
        
        double result = tennis.getCost();
        assertEquals(33, result);
        //tennis.
    }
     @Test
    public void testNonMember3()
    {
        tennis = new Tennis();
        tennis.setNumberMembers(3);
        tennis.setIsMember(false);
        tennis.setLesson(1);
        tennis.calculateCost();
        
        double result = tennis.getCost();
        assertEquals(23, result);
        //tennis.
    }
    @Test
    public void testNonMember4()
    {
        tennis = new Tennis();
        tennis.setNumberMembers(4);
        tennis.setIsMember(false);
        tennis.setLesson(1);
        tennis.calculateCost();
        
        double result = tennis.getCost();
        assertEquals(18, result);
        //tennis.
    }
}
